import React from 'react';
import {
  Input,
  Container,
  Card,
  CardBody,
  CardTitle,
  Button,
  Row,
  Alert,
  Col,
  Form,
  FormGroup,
} from 'reactstrap';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      alert: <></>,
    };
  }

  createUser = () => {
    if (
      this.state.email === '' ||
      this.state.password === '' ||
      this.state.name === '' ||
      this.state.firstName === ''
    ) {
      this.setState({
        alert: <Alert color="danger">Please, fill all the fields</Alert>,
      });
    } else if (
      JSON.parse(localStorage.getItem('users')).find(user => user.email === this.state.email)
    ) {
      this.setState({
        alert: <Alert color="danger">An account with this email already exists</Alert>,
      });
    } else if (!this.validateEmail(this.state.email)) {
      this.setState({
        alert: <Alert color="danger">This is not a valid email</Alert>,
      });
    } else {
      const users = JSON.parse(localStorage.getItem('users'));
      const wallets = JSON.parse(localStorage.getItem('wallets'));
      const lastId = users[users.length - 1].id;
      const lastWalletId = wallets[wallets.length - 1].id;
      users.push({
        id: lastId + 1,
        first_name: this.state.firstName,
        last_name: this.state.lastName,
        email: this.state.email,
        password: this.state.password,
        is_admin: false,
      });
      wallets.push({
        user_id: lastId + 1,
        id: lastWalletId + 1,
        balance: 0,
      });
      localStorage.setItem('users', JSON.stringify(users));
      localStorage.setItem('wallets', JSON.stringify(wallets));
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/');
    }
  };

  // eslint-disable-next-line class-methods-use-this
  validateEmail(email) {
    // eslint-disable-next-line no-useless-escape
    const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
  }

  handleOnChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleOnChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleOnChangeLastName(event) {
    this.setState({
      lastName: event.target.value,
    });
  }

  handleOnChangeFirstName(event) {
    this.setState({
      firstName: event.target.value,
    });
  }

  render() {
    return (
      <Container>
        <Card className="card-layout">
          <CardBody>
            <CardTitle className="card-title">
              <div className="title">
                <span className="water">New</span>
                <span className="melon">User</span>
              </div>
            </CardTitle>
            <Form>
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  placeholder="Email"
                  onChange={e => this.handleOnChangeEmail(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  name="lastName"
                  placeholder="Last name"
                  onChange={e => this.handleOnChangeLastName(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  name="first-name"
                  placeholder="First name"
                  onChange={e => this.handleOnChangeFirstName(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  name="password"
                  placeholder="Password"
                  onChange={e => this.handleOnChangePassword(e)}
                />
              </FormGroup>
            </Form>
            <Row>
              <Col>
                <Button id="button" color="primary" onClick={this.createUser}>
                  Create Account
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>{this.state.alert}</Col>
            </Row>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

export default SignUp;
