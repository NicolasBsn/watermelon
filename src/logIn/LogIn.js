import React from 'react';
import './LogIn.css';
import {
  Input,
  Container,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Alert,
  Row,
  Col,
  Form,
  FormGroup,
} from 'reactstrap';

export default class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      alert: <></>,
      usersList: JSON.parse(localStorage.getItem('users')),
    };
  }

  // eslint-disable-next-line react/sort-comp
  handleOnChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleOnChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  signIn() {
    const user = this.state.usersList.find(
      u => u.email === this.state.email && u.password === this.state.password
    );
    if (user != null) {
      this.setState({
        alert: <Alert color="success">Accepted</Alert>,
      });
      // Router sur le dashboard
      localStorage.setItem('user', JSON.stringify(user));
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/homepage');
    } else {
      this.setState({
        alert: <Alert color="danger">Refused, try again</Alert>,
      });
    }
  }

  setRedirect() {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/signup');
  }

  render() {
    return (
      <Container>
        <Card className="card-layout">
          <CardBody>
            <CardTitle className="card-title">
              <div className="title">
                <span className="water">Water</span>
                <span className="melon">melon</span>
              </div>
            </CardTitle>
            <CardSubtitle className="card-subtitle">Identification</CardSubtitle>
            <Form>
              <FormGroup>
                <Input
                  type="email"
                  name="email"
                  id="exampleEmail"
                  placeholder="Email"
                  onChange={e => this.handleOnChangeEmail(e)}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  id="examplePassword"
                  placeholder="Password"
                  onChange={e => this.handleOnChangePassword(e)}
                />
              </FormGroup>
            </Form>
            <Row>
              <Col>
                <Button id="button" color="primary" onClick={() => this.signIn()}>
                  Sign in
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button id="button" color="light" onClick={() => this.setRedirect()}>
                  New user
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>{this.state.alert}</Col>
            </Row>
          </CardBody>
        </Card>
      </Container>
    );
  }
}
