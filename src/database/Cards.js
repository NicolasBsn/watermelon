export default [
  {
    user_id: 1,
    id: 1,
    last_4: '0000',
    brand: 'Visa',
    expired_at: '31/12/2021',
  },
  {
    user_id: 1,
    id: 2,
    last_4: '0001',
    brand: 'MasterCard',
    expired_at: '31/12/2021',
  },
];
