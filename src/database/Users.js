export default [
  {
    id: 1,
    first_name: 'Nicolas',
    last_name: 'B.',
    email: 'nicolas@test.fr',
    password: 'test',
    is_admin: 'false',
  },
  {
    id: 2,
    first_name: 'Olivia',
    last_name: 'D.',
    email: 'olivia@test.fr',
    password: 'test1',
    is_admin: 'false',
  },
];
