import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import LogIn from './logIn/LogIn';
import SignUp from './logIn/SignUp';
import HomePage from './homePage/HomePage';
import ModifyAccount from './homePage/ModifyAccount';
import AppCards from './homePage/AppCards';
import AddCards from './homePage/AddCards';
import ModifyCards from './homePage/ModifyCards';
import users from './database/Users';
import wallets from './database/Wallet';
import payins from './database/Payins';
import payouts from './database/Payouts';
import transfers from './database/Transfers';
import cards from './database/Cards';

if (localStorage.getItem('users') === null) {
  localStorage.setItem('users', JSON.stringify(users));
}
if (localStorage.getItem('wallets') === null) {
  localStorage.setItem('wallets', JSON.stringify(wallets));
}
if (localStorage.getItem('payins') === null) {
  localStorage.setItem('payins', JSON.stringify(payins));
}
if (localStorage.getItem('payouts') === null) {
  localStorage.setItem('payouts', JSON.stringify(payouts));
}
if (localStorage.getItem('transfers') === null) {
  localStorage.setItem('transfers', JSON.stringify(transfers));
}
if (localStorage.getItem('cards') === null) {
  localStorage.setItem('cards', JSON.stringify(cards));
}

const routing = (
  <Router>
    <Switch>
      <Route exact path="/" component={LogIn} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/homepage" component={HomePage} />
      <Route exact path="/modify-account" component={ModifyAccount} />
      <Route exact path="/cards" component={AppCards} />
      <Route exact path="/add-cards" component={AddCards} />
      <Route exact path="/modify-cards" component={ModifyCards} />
    </Switch>
  </Router>
);
// eslint-disable-next-line no-undef
ReactDOM.render(routing, document.getElementById('root'));
