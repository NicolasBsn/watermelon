/* eslint-disable react/prop-types */
import React from 'react';
import { Card, Input, CardTitle, Col, Row, Button } from 'reactstrap';

export default class Transfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      email: '',
    };
  }

  handleOnChangeAmount = event => {
    this.setState({
      amount: event.target.value,
    });
  };

  handleOnChangeEmail = event => {
    this.setState({
      email: event.target.value,
    });
  };

  handleTransferAmount = () => {
    this.props.transferAmount(this.state.amount, this.state.email);
    this.setState({
      amount: '',
    });
  };

  render() {
    return (
      <Card>
        <CardTitle>Transfer money</CardTitle>
        <Row>
          <Col xs="3">
            <Input
              type="number"
              value={this.state.amount}
              onChange={e => this.handleOnChangeAmount(e)}
            />
          </Col>
          <Col xs="1">€</Col>
          <Col xs="6">
            <Input
              type="email"
              value={this.state.email}
              placeholder="Email of the other person"
              onChange={e => this.handleOnChangeEmail(e)}
            />
          </Col>
        </Row>
        <Row>
          <Col xs="3">
            <Button onClick={this.handleTransferAmount}>Confirm</Button>
          </Col>
        </Row>
      </Card>
    );
  }
}
