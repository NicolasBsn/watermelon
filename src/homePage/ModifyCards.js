import React from 'react';
import './HomePage.css';
import { Button, Card, Alert, Input, Form, Col, Row, CardTitle } from 'reactstrap';

export default class Cards extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnChangeBrand = this.handleOnChangeBrand.bind(this);
    this.state = {
      cards: JSON.parse(localStorage.getItem('cards')),
      brand: '',
      alert: false,
    };
  }

  redirectCards = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/cards');
  };

  disconnect = () => {
    localStorage.removeItem('user');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/');
  };

  removeCard = () => {
    const cards = this.state.cards.filter(
      c => c.id !== JSON.parse(localStorage.getItem('card')).id
    );
    localStorage.setItem('cards', JSON.stringify(cards));
    localStorage.removeItem('card');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/cards');
  };

  modifyCard = () => {
    if (this.state.brand === '') {
      this.setState({
        alert: true,
      });
    } else {
      const card = this.state.cards.find(c => c.id === JSON.parse(localStorage.getItem('card')).id);
      card.brand = this.state.brand;
      localStorage.setItem('card', JSON.stringify(card));
      localStorage.setItem('cards', JSON.stringify(this.state.cards));
      localStorage.removeItem('card');
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/cards');
    }
  };

  handleOnChangeBrand(event) {
    this.setState({ brand: event.target.value });
  }

  render() {
    let alert = <></>;
    if (this.state.alert) {
      alert = <Alert color="danger">Please, choose a brand name</Alert>;
    }
    return (
      <div>
        <ul>
          <li>
            <Button onClick={this.redirectCards} color="info" id="#button">
              Your Cards
            </Button>
          </li>
          <li>
            <Button onClick={this.disconnect} color="danger" id="#button">
              Log out
            </Button>
          </li>
        </ul>
        <Col>
          <Row>
            <Card>
              <CardTitle>Select the new brand of your card here !</CardTitle>
              Your current card last four numbers {
                JSON.parse(localStorage.getItem('card')).last_4
              }{' '}
              brand is {JSON.parse(localStorage.getItem('card')).brand}
              <Form>
                <Input
                  onChange={e => this.handleOnChangeBrand(e)}
                  type="select"
                  name="select"
                  id="exampleSelect"
                >
                  <option>Select a brand</option>
                  <option>Visa</option>
                  <option>MasterCard</option>
                  <option>AmericanExpress</option>
                  <option>Union_pay</option>
                  <option>jcb</option>
                </Input>
                <Button onClick={this.modifyCard}>Send</Button>
                {alert}
              </Form>
            </Card>
          </Row>
          <Row>
            <Card>
              <CardTitle>Click on Delete if you want to delete your card</CardTitle>
              <Form>
                <Button onClick={this.removeCard}>Delete</Button>
              </Form>
            </Card>
          </Row>
        </Col>
      </div>
    );
  }
}
