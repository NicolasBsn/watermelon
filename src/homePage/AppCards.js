import React from 'react';
import { Button, Card, CardText, CardTitle } from 'reactstrap';

export default class AppCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardsList: JSON.parse(localStorage.getItem('cards')).filter(
        c => c.user_id === JSON.parse(localStorage.getItem('user')).id
      ),
    };
    this.displayList = this.displayList.bind(this);
  }

  modifyCard = card => {
    localStorage.setItem('card', JSON.stringify(card));
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/modify-cards');
  };

  redirectHomepage = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/homepage');
  };

  disconnect = () => {
    localStorage.removeItem('user');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/');
  };

  addCard = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/add-cards');
  };

  displayList(card) {
    return (
      <Card key={card.id}>
        <CardTitle> Card last four numbers : {card.last_4}</CardTitle>
        <CardText>
          brand : {card.brand}, expired at :{card.expired_at}
        </CardText>
        <Button onClick={() => this.modifyCard(card)} color="info" id="button">
          Modify
        </Button>
      </Card>
    );
  }

  render() {
    return (
      <div className="AppCards">
        <ul>
          <li>
            <Button onClick={this.redirectHomepage} color="primary" id="#button">
              Homepage
            </Button>
          </li>
          <li>
            <Button onClick={this.disconnect} color="danger" id="#button">
              Log out
            </Button>
          </li>
          <li>
            <Button onClick={this.addCard} color="secondary" id="button">
              Add a Card
            </Button>
          </li>
        </ul>
        {this.state.cardsList.map(this.displayList)}
      </div>
    );
  }
}
