import React from 'react';
import {
  Input,
  Container,
  Card,
  CardBody,
  CardTitle,
  Button,
  Row,
  Col,
  Form,
  Alert,
  FormGroup,
} from 'reactstrap';

import './HomePage.css';

export default class LogIn extends React.Component {
  constructor(props) {
    super(props);
    const user = JSON.parse(localStorage.getItem('user'));
    this.state = {
      email: user.email,
      password: user.password,
      firstName: user.first_name,
      lastName: user.last_name,
      alert: <></>,
      usersList: JSON.parse(localStorage.getItem('users')),
    };
  }

  redirectHomepage = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/homepage');
  };

  disconnect = () => {
    localStorage.removeItem('user');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/');
  };

  modifyAccount = () => {
    if (
      this.state.email === '' ||
      this.state.password === '' ||
      this.state.name === '' ||
      this.state.firstName === ''
    ) {
      this.setState({
        alert: <Alert color="danger">Please, fill all the fields</Alert>,
      });
    } else if (
      JSON.parse(localStorage.getItem('users')).find(
        user =>
          user.email === this.state.email && user.id !== JSON.parse(localStorage.getItem('user')).id
      )
    ) {
      this.setState({
        alert: <Alert color="danger">An account with this email already exists</Alert>,
      });
    } else if (!this.validateEmail(this.state.email)) {
      this.setState({
        alert: <Alert color="danger">This is not a valid email</Alert>,
      });
    } else {
      const user = this.state.usersList.find(
        u => JSON.parse(localStorage.getItem('user')).id === u.id
      );

      user.first_name = this.state.firstName;
      user.last_name = this.state.lastName;
      user.email = this.state.email;
      user.password = this.state.password;

      localStorage.setItem('user', JSON.stringify(user));
      localStorage.setItem('users', JSON.stringify(this.state.usersList));
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/homepage');
    }
  };

  // eslint-disable-next-line class-methods-use-this
  validateEmail(email) {
    // eslint-disable-next-line no-useless-escape
    const pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
  }

  handleOnChangeEmail(event) {
    this.setState({
      email: event.target.value,
    });
  }

  handleOnChangePassword(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleOnChangeLastName(event) {
    this.setState({
      lastName: event.target.value,
    });
  }

  handleOnChangeFirstName(event) {
    this.setState({
      firstName: event.target.value,
    });
  }

  render() {
    if (localStorage.getItem('user') === null) {
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/');
    }
    return (
      <div>
        <ul>
          <li>
            <Button onClick={this.redirectHomepage} color="primary" id="#button">
              Homepage
            </Button>
          </li>
          <li>
            <Button onClick={this.disconnect} color="danger" id="#button">
              Log out
            </Button>
          </li>
        </ul>
        <Container>
          <Card className="card-layout">
            <CardBody>
              <CardTitle className="card-title">
                <div className="title">
                  <span className="water">Modify your account</span>
                </div>
              </CardTitle>
              <Form>
                <FormGroup>
                  <Input
                    type="email"
                    name="email"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={e => this.handleOnChangeEmail(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    name="lastName"
                    placeholder="Last name"
                    value={this.state.lastName}
                    onChange={e => this.handleOnChangeLastName(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    name="first-name"
                    placeholder="First name"
                    value={this.state.firstName}
                    onChange={e => this.handleOnChangeFirstName(e)}
                  />
                </FormGroup>
                <FormGroup>
                  <Input
                    name="password"
                    placeholder="Password"
                    value={this.state.password}
                    onChange={e => this.handleOnChangePassword(e)}
                  />
                </FormGroup>
              </Form>
              <Row>
                <Col>
                  <Button id="button" color="primary" onClick={this.modifyAccount}>
                    Modify your account
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col>{this.state.alert}</Col>
              </Row>
            </CardBody>
          </Card>
        </Container>
      </div>
    );
  }
}
