import React from 'react';
import { Card, CardText, CardTitle } from 'reactstrap';

export default class Deposit extends React.Component {
  render() {
    return (
      <Card>
        <CardTitle>Your balance</CardTitle>
        <CardText>
          {
            // eslint-disable-next-line react/prop-types
            this.props.wallets.find(
              wallet => wallet.user_id === JSON.parse(localStorage.getItem('user')).id
            ).balance
          }
          €
        </CardText>
      </Card>
    );
  }
}
