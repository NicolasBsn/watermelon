/* eslint-disable react/prop-types */
import React from 'react';
import { Card, Input, CardTitle, Col, Row, Button } from 'reactstrap';

export default class Deposit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: '',
    };
  }

  handleOnChangeAmount = event => {
    this.setState({
      amount: event.target.value,
    });
  };

  handleDecreaseMount = () => {
    this.props.decreaseAmount(this.state.amount);
    this.setState({
      amount: '',
    });
  };

  render() {
    return (
      <Card>
        <CardTitle>Withdrawal</CardTitle>
        <Row>
          <Col xs="3">
            <Input
              type="number"
              value={this.state.amount}
              onChange={e => this.handleOnChangeAmount(e)}
            />
          </Col>
          <Col xs="1">€</Col>
          <Col xs="3">
            <Button onClick={this.handleDecreaseMount}>Confirm</Button>
          </Col>
        </Row>
      </Card>
    );
  }
}
