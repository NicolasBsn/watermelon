import React from 'react';
import { Container, Button, Row, Col, Alert } from 'reactstrap';
import Deposit from './Deposit';
import Withdrawal from './Withdrawal';
import DisplayWallet from './DisplayWallet';
import Transfer from './Transfer';
import './HomePage.css';

export default class LogIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      walletsList: JSON.parse(localStorage.getItem('wallets')),
      alert: false,
      alertTransfer: false,
      alertTransferUser: false,
    };
  }

  modifyAccount = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/modify-account');
  };

  cards = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/cards');
  };

  disconnect = () => {
    localStorage.removeItem('user');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/');
  };

  payin = money => {
    // eslint-disable-next-line react/no-access-state-in-setstate
    const wallet = this.state.walletsList.find(
      u => JSON.parse(localStorage.getItem('user')).id === u.user_id
    );

    wallet.balance = Number(wallet.balance) + Number(money);

    localStorage.setItem('wallets', JSON.stringify(this.state.walletsList));
    const payins = JSON.parse(localStorage.getItem('payins'));
    payins.push({
      id: payins[payins.length - 1].id + 1,
      wallet_id: wallet.id,
      amount: Number(money),
    });
    localStorage.setItem('payins', JSON.stringify(payins));

    this.setState({
      walletsList: JSON.parse(localStorage.getItem('wallets')),
      alert: false,
    });
  };

  payout = money => {
    const wallet = this.state.walletsList.find(
      u => JSON.parse(localStorage.getItem('user')).id === u.user_id
    );

    if (Number(wallet.balance) - Number(money) >= 0) {
      wallet.balance = Number(wallet.balance) - Number(money);

      localStorage.setItem('wallets', JSON.stringify(this.state.walletsList));
      const payouts = JSON.parse(localStorage.getItem('payouts'));
      payouts.push({
        id: payouts[payouts.length - 1].id + 1,
        wallet_id: wallet.id,
        amount: Number(money),
      });
      localStorage.setItem('payouts', JSON.stringify(payouts));

      this.setState({
        alert: false,
        walletsList: JSON.parse(localStorage.getItem('wallets')),
      });
    } else {
      this.setState({
        alert: true,
      });
    }
  };

  transfer = (money, email) => {
    this.setState({
      alert: false,
      alertTransfer: false,
      alertTransferUser: false,
    });
    const debitedWallet = this.state.walletsList.find(
      u => JSON.parse(localStorage.getItem('user')).id === u.user_id
    );

    const creditedUser = JSON.parse(localStorage.getItem('users')).find(u => email === u.email);

    if (
      Number(debitedWallet.balance) - Number(money) >= 0 &&
      creditedUser !== undefined &&
      creditedUser.id !== JSON.parse(localStorage.getItem('user')).id
    ) {
      const creditedWallet = this.state.walletsList.find(u => creditedUser.id === u.user_id);
      debitedWallet.balance = Number(debitedWallet.balance) - Number(money);
      creditedWallet.balance = Number(creditedWallet.balance) + Number(money);
      localStorage.setItem('wallets', JSON.stringify(this.state.walletsList));
      const transfers = JSON.parse(localStorage.getItem('transfers'));
      transfers.push({
        id: transfers[transfers.length - 1].id + 1,
        credited_wallet_id: creditedWallet.id,
        debited_wallet_id: debitedWallet.id,
        amount: Number(money),
      });
      localStorage.setItem('transfers', JSON.stringify(transfers));

      this.setState({
        alert: false,
        alertTransfer: false,
        alertTransferUser: false,
        walletsList: JSON.parse(localStorage.getItem('wallets')),
      });
    } else if (Number(debitedWallet.balance) - Number(money) < 0) {
      this.setState({
        alert: true,
      });
    } else if (creditedUser === undefined) {
      this.setState({
        alertTransfer: true,
      });
    } else if (creditedUser.id === JSON.parse(localStorage.getItem('user')).id) {
      this.setState({
        alertTransferUser: true,
      });
    }
  };

  render() {
    let alert = <></>;
    let alertTransfer = <></>;
    let alertTransferUser = <></>;
    if (localStorage.getItem('user') === null) {
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/');
    }
    if (this.state.alert) {
      alert = <Alert color="danger">You do not have enough money</Alert>;
    }
    if (this.state.alertTransfer) {
      alertTransfer = <Alert color="danger">This user does not exist</Alert>;
    }
    if (this.state.alertTransferUser) {
      alertTransferUser = <Alert color="danger">You can not send money to you</Alert>;
    }

    return (
      <div>
        <ul>
          <li>
            <Button onClick={this.modifyAccount} color="primary" id="#button">
              Modify your account
            </Button>
          </li>
          <li>
            <Button onClick={this.cards} color="info" id="#button">
              Your Cards
            </Button>
          </li>
          <li>
            <Button onClick={this.disconnect} color="danger" id="#button">
              Log out
            </Button>
          </li>
        </ul>
        <Container>
          <Row>
            <Col xs="12" sm="6">
              <DisplayWallet wallets={this.state.walletsList} />
            </Col>
            <Col xs="12" sm="6">
              <Deposit addAmount={this.payin} amount={this.state.amount} />
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6">
              <Withdrawal decreaseAmount={this.payout} amount={this.state.amount} />
            </Col>
            <Col xs="12" sm="6">
              {alert}
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6">
              <Transfer transferAmount={this.transfer} amount={this.state.amount} />
            </Col>
            <Col xs="12" sm="6">
              {alertTransfer}
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6">
              {alertTransferUser}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
