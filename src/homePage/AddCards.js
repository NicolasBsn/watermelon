import React from 'react';
import { Button, Card, Alert, Form, Input, CardTitle } from 'reactstrap';

export default class Cards extends React.Component {
  constructor(props) {
    super(props);
    this.state = { brand: '', alert: false };
    this.handleOnChangeBrand = this.handleOnChangeBrand.bind(this);
    this.getCurrentDate = this.getCurrentDate.bind(this);
  }

  createCard = () => {
    if (this.state.brand === '') {
      this.setState({
        alert: true,
      });
    } else {
      const users = JSON.parse(localStorage.getItem('users'));
      const cards = JSON.parse(localStorage.getItem('cards'));
      const lastId = users[users.length - 1].id;
      const lastCardsId = cards[cards.length - 1].id;
      const calcul = parseInt(Math.random() * (9999 - 0) + 0, 10);
      const number = `${calcul}`;
      const date = this.getCurrentDate();
      cards.push({
        user_id: lastId,
        id: lastCardsId + 1,
        brand: this.state.brand,
        last_4: number,
        expired_at: date,
      });
      localStorage.setItem('cards', JSON.stringify(cards));
      // eslint-disable-next-line react/prop-types
      this.props.history.push('/cards');
    }
  };

  getCurrentDate = () => {
    // Expired date : our currently year + 3 years
    const newDate = new Date();
    const date = newDate.getDate();
    const month = newDate.getMonth();
    const year = newDate.getFullYear() + 3;

    return `${date}/${month}/${year}`;
  };

  disconnect = () => {
    localStorage.removeItem('user');
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/');
  };

  redirectCards = () => {
    // eslint-disable-next-line react/prop-types
    this.props.history.push('/cards');
  };

  handleOnChangeBrand(event) {
    this.setState({ brand: event.target.value });
  }

  render() {
    let alert = <></>;
    if (this.state.alert) {
      alert = <Alert color="danger">Please, choose a brand name</Alert>;
    }
    return (
      <div>
        <ul>
          <li>
            <Button onClick={this.redirectCards} color="info" id="#button">
              Your Cards
            </Button>
          </li>
          <li>
            <Button onClick={this.disconnect} color="danger" id="#button">
              Log out
            </Button>
          </li>
        </ul>
        <Card>
          <CardTitle>Select the new brand of your card</CardTitle>
          <Form>
            <Input
              onChange={e => this.handleOnChangeBrand(e)}
              type="select"
              name="select"
              id="exampleSelect"
            >
              <option>Select a brand</option>
              <option>Visa</option>
              <option>MasterCard</option>
              <option>AmericanExpress</option>
              <option>Union_pay</option>
              <option>jcb</option>
            </Input>
            <Button onClick={this.createCard}>Send</Button>
            {alert}
          </Form>
        </Card>
      </div>
    );
  }
}
